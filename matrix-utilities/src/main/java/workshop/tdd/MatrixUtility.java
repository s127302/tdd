package workshop.tdd;

public class MatrixUtility implements Utility {

    public static int[][] sum(int[][] first, int[][] second) {
        return first;
    }

    public static int[][] transpose(int[][] matrix) {

        int[][] result = new int[matrix[0].length][matrix.length];
        return result;
    }

    public static Matrix transpose(Matrix matrix) {
        int[][] resultMatrix = new int[matrix.getCols()][matrix.getRows()];
        return new Matrix(resultMatrix);
    }

    public static int[][] sub(int[][] matrix, int row, int col) {
        return new int[0][];
    }

    public static int[][] scale(int[][] matrix, int scalar) {
        int[][] result = new int[matrix.length][matrix[0].length];
        return result;
    }

    public static Matrix scale(Matrix matrix, int scalar) {
        int[][] result = new int[matrix.getRows()][matrix.getCols()];
        return new Matrix(result);
    }


    public static Matrix copySquareMatrix(Matrix matrix, Object o) {
        return matrix;
    }

    public static int determinant(Matrix matrix) {
        return 0;
    }
}
