package workshop.tdd;

public enum CopyType {
    DIAGONAL, UPPER, LOWER;
}
